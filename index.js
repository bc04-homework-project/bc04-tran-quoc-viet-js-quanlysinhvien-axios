const BASE_URL = "https://62db6cb7e56f6d82a772862e.mockapi.io/";

function getDSSV() {
  batLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "GET",
  })
    .then(function (res) {
      // console.log(res);
      tatLoading();
      renderDSSV(res.data);
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}
getDSSV();

function xoaSinhVien(id) {
  batLoading();
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      console.log(res);
      getDSSV();
      tatLoading();
    })
    .catch(function (err) {
      console.log(err);
      tatLoading();
    });
}

function themSV() {
  let sv = layThongTinTuForm();
  let newSV = {
    name: sv.ten,
    email: sv.email,
    password: sv.matKhau,
    math: sv.toan,
    physics: sv.ly,
    chemistry: sv.hoa,
  };

  batLoading();

  axios({
    url: `${BASE_URL}/sv`,
    method: "POST",
    data: newSV,
  })
    .then(function (res) {
      console.log(res);
      getDSSV();
      tatLoading();
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}

function suaSinhVien(id) {
  batLoading();
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "GET",
  })
    .then(function (res) {
      console.log(res);
      showThongTinLenForm(res.data);
      tatLoading();
    })
    .catch(function (err) {
      console.log(err);
      tatLoading();
    });
}

function capNhatSV() {
  let sv = layThongTinTuForm();
  let maSv = document.getElementById("txtMaSV").value;

  let fixSV = {
    name: sv.ten,
    email: sv.email,
    password: sv.matKhau,
    math: sv.toan,
    physics: sv.ly,
    chemistry: sv.hoa,
  };

  batLoading();

  axios({
    url: `${BASE_URL}/sv/${maSv}`,
    method: "PUT",
    data: fixSV,
  })
    .then(function (res) {
      console.log(res);
      getDSSV();
      tatLoading();
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}
